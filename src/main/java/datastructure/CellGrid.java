package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    private int rows = 0;
    private int cols = 0;
    private CellState grid[][]; 
    
    
    
    public CellGrid(int rows, int columns, CellState initialState) {
		
        this.rows = rows; 
        this.cols = columns;
        grid = new CellState[rows][columns];

        
        for(int row = 0; row<numRows(); row++){
            for(int col = 0; col<numColumns();col++){
                grid[row][col] = initialState;

            }
        }
        
        
    }

    @Override
    public int numRows() {
        
        return rows;
    }

    @Override
    public int numColumns() {
        
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        
        if (0 <= row && row < numRows() && 0 <= column && column < numColumns()){
            grid[row][column] = element;
        } else {
            throw new IndexOutOfBoundsException();
        
        }
    }

    @Override
    public CellState get(int row, int column) {
       
        if (0 <= row && row < numRows() && 0 <= column && column < numColumns()){
            return grid[row][column];
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public IGrid copy() {
        
        IGrid newGrid = new CellGrid(numRows(), numColumns(), CellState.ALIVE);
        for(int row = 0; row<numRows(); row++){
            for(int col = 0; col<numColumns(); col ++){
                newGrid.set(row, col, get(row, col));
            }
        }
        return newGrid;
    }
    
}
